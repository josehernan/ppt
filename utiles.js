//conjunto de utilidades

/*
0 Piedra
1 Papel
2 Tijejas
3 Lagarto
4 Spock
*/

let tabla = [
	{"ganador": 0, "perdedores": [2,3] },
	{"ganador": 1, "perdedores": [0,4] },
	{"ganador": 2, "perdedores": [1,3] },
	{"ganador": 3, "perdedores": [1,4] },
	{"ganador": 4, "perdedores": [0,2] }
]

getGanadores = (v) => {
	var ganadores = []
	tabla.map( (e) => {
		if(e.perdedores.includes(v)) {
			ganadores.push(e.ganador)
		}
	})	
	return ganadores
}

getPerdedores = (v) => {
	g = tabla.filter( (e) => { return e.ganador === v } )
	return g[0].perdedores
} 

esGanador = (a, b) => {
	g = tabla.filter( (e) => { return e.ganador === a } )
	if( g[0].perdedores.includes(b) ) {
		return true
	} else {
		return false
	}
}

esPerdedor = (a, b) => {
	if( (getGanadores(a)).includes(b) ) {
		return true
	} else {
		return false
	}
}

agregarElemento = (padre, hijo) => {
    var p = document.querySelector(padre)
    p.insertAdjacentHTML( 'beforeend', hijo)
}

setStyles = (elemento, styles) => {
	var el = document.querySelector(elemento)
	if(el !== null) {
		for (var property in styles) {
			el.style[property] = styles[property]
		}
	}
}

setStylesAll = (elemento, styles) => {
	var els = Array.prototype.slice.call(document.querySelectorAll(elemento))
	if(els !== null) {
		for (var i=0; i < els.length; i++) {
			for (var property in styles) {
				els[i].style[property] = styles[property]
			}
		}
	}
}

function openFullscreen() {
var elem = document.documentElement;
  if (elem.requestFullscreen) {
    elem.requestFullscreen();
  } else if (elem.mozRequestFullScreen) { /* Firefox */
    elem.mozRequestFullScreen();
  } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari & Opera */
    elem.webkitRequestFullscreen();
  } else if (elem.msRequestFullscreen) { /* IE/Edge */
    elem.msRequestFullscreen();
  }
}

function closeFullscreen() {
  if (document.exitFullscreen) {
    document.exitFullscreen();
  } else if (document.mozCancelFullScreen) {
    document.mozCancelFullScreen();
  } else if (document.webkitExitFullscreen) {
    document.webkitExitFullscreen();
  } else if (document.msExitFullscreen) {
    document.msExitFullscreen();
  }
}

let pausa = false
let pausaLandscape = false

window.addEventListener("orientationchange", function() {
  console.log("the orientation of the device is now " + screen.orientation.angle);
  
  if (screen.orientation.angle !== 0) {
		//openFullscreen()
		pausaLandscape = true	
  }
  else {
		//closeFullscreen()
		pausaLandscape = false
	}
	
})

esMobile = () => { 
 if( navigator.userAgent.match(/Android/i)
	|| navigator.userAgent.match(/webOS/i)
	|| navigator.userAgent.match(/iPhone/i)
	|| navigator.userAgent.match(/iPad/i)
	|| navigator.userAgent.match(/iPod/i)
	|| navigator.userAgent.match(/BlackBerry/i)
	|| navigator.userAgent.match(/Windows Phone/i)
	) { return true } else { return false }
}

normalizarAcentos = (cadena) => {
	var chars={
		"á":"a", "é":"e", "í":"i", "ó":"o", "ú":"u",
		"à":"a", "è":"e", "ì":"i", "ò":"o", "ù":"u", "ñ":"n",
		"Á":"A", "É":"E", "Í":"I", "Ó":"O", "Ú":"U",
		"À":"A", "È":"E", "Ì":"I", "Ò":"O", "Ù":"U", "Ñ":"N"}
	var expr=/[áàéèíìóòúùñ]/ig;
	var res=cadena.replace(expr,function(e){return chars[e]})
	return res
}

App42.initialize("16a4fc5caf6530644fdfc09f096d69bad1a7bbdf5d20b9ea246a863841462860","01bbc126435496a305071aa27dd7f2ef40e31f0cd5e4261f2626237683dad43b")

var scoreBoardService  = new App42ScoreBoard()

API_mandarPuntaje = (nombreJuego,nombreJugador, puntaje) => {
	scoreBoardService.saveUserScore(nombreJuego,nombreJugador,puntaje,{
		success: function(object) {
			var game = JSON.parse(object)
			result = game.app42.response.games.game
			console.log(result)
		},
		error: function(error) { console.log(error) }
	})
}

API_traerPuntajes = (nombreJuego, recargarEnError = false) => {
	return new Promise(resolve => {
		scoreBoardService.getTopNRankings(nombreJuego,10,{
			success: function(object) {
				var game = JSON.parse(object)
				result = game.app42.response.games.game
				console.log(nombreJuego)						
				console.log(result)
				lista = result.scores.score
				resolve(lista)
			},
			error: function(error) { console.log(error) 
				if(recargarEnError) { location.reload() }
				resolve(false)
			}
		})
	})
}

let API_puntajeMinimoModo3
let API_puntajeMinimoModo5

API_traerPuntajeMinimo = async() => {
	do{	
		API_puntajesModo3 = await API_traerPuntajes("modo3")
		API_puntajeMinimoModo3 = API_puntajesModo3[API_puntajesModo3.length -1].value
		
		API_puntajesModo5 = await API_traerPuntajes("modo5")
		API_puntajeMinimoModo5 = API_puntajesModo5[API_puntajesModo5.length -1].value
	}while(!API_puntajesModo3 && !API_puntajesModo5)
	console.log('API_traerPuntajeMinimo ' + API_puntajeMinimoModo3 + ' ' + API_puntajeMinimoModo5)
}
