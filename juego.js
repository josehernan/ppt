/*
VER
puntajes, mínimo si no hay valor, fallo API
controles a los 2 costados
fullscreen
vibración
*/

/*
    _ _    ___    / _` |   | |    __ _     ___   
   | '_|  / -_)   \__, |   | |   / _` |   (_-<   
  _|_|_   \___|   |___/   _|_|_  \__,_|   /__/_  
_|"""""|_|"""""|_|"""""|_|"""""|_|"""""|_|"""""| 
"`-0-0-'"`-0-0-'"`-0-0-'"`-0-0-'"`-0-0-'"`-0-0-'

Las tijeras cortan el papel, 
el papel cubre a la piedra, 
la piedra aplasta al lagarto, 
el lagarto envenena a Spock, 
Spock destroza las tijeras, 
las tijeras decapitan al lagarto, 
el lagarto se come el papel, 
el papel refuta a Spock, 
Spock vaporiza la piedra,
la piedra aplasta las tijeras.
*/

////////////////////////////////////////////////////////////////////////

juegoPC = () => {
	var w = window,
	d = document,
	e = d.documentElement,
	g = d.getElementsByTagName('body')[0],
	window_width = w.innerWidth||e.clientWidth||g.clientWidth,
	window_height = w.innerHeight||e.clientHeight||g.clientHeight
	if(window_height < window_width) {
		alert("redimensionar")
	}
}

if( !esMobile() ) { juegoPC() }

API_traerPuntajeMinimo()

let sprites = new Image()
sprites.src = './sprites.png'
	//sprites.onload = () => { cargarMetas()}

let metasSprites = new Image()
//cargarMetas = () => {
metasSprites.src = './metas.png'
	//metasSprites.onload = () => { gameLoop() }
	//}

let iconoStart = { "velocidad": 0, "x": 0, "y":0, "dir_x": 1, "dir_y": 1, "imagen": new Image() }
iconoStart.imagen.src = './start.png'


////////////////////////////////////////////////////////////////////////

let juego = { "velocidad": "", "velocidadInicial": "", "velocidadFinal": "", "delta": "", "puntaje": 0, "vidas": 3, "modo": "", "activo": true, "mecanica": "simbolos" }
let pantalla = {"x": 0, "y" :0, "lado": "", "orientacion": "", "relacion": ""}

iniciarJuego = (m) => {
	//fullscreen opcional
	//openFullscreen()
	juego.modo = m
	juego.activo = true
	juego.puntaje = 0
	juego.vidas = 3
	pausaLandscape = false
	pausa = false
	//setStyles('#contenedorMenu', {'display': 'none'})
	if(document.querySelector("#contenedorMenu") !== null) {
		document.getElementById("contenedorMenu").remove()
	}

	if( document.querySelector("#contenedorPrincipal") !== null ) {
		document.getElementById("contenedorPrincipal").remove()
	}
	
	desplegarJuego()
}

////////////////////////////////////////////////////////////////////////
// MENU
setStyles('body', {'display': 'flex'})
agregarElemento('body', '<div id="contenedorMenu"></div>')
agregarElemento('#contenedorMenu', '<p class="textos">Piedra, Papel, Tijetas...</p>')
agregarElemento('#contenedorMenu', '<div class="btn-modo3">modo 3</div>')
agregarElemento('#contenedorMenu', '<div class="btn-modo5">modo 5</div><br />')
agregarElemento('#contenedorMenu', '<a href=reglas.html><div class="btn-reglas">Reglas</div></a><br />')
agregarElemento('#contenedorMenu', '<a href=puntajes.html><div class="btn-puntajes">Puntajes</div></a><br />')
agregarElemento('#contenedorMenu', '<a href=creditos.html><div class="btn-creditos">Créditos</div></a>')

document.querySelector('.btn-modo3').addEventListener('click', () => { iniciarJuego(3) }, false)
document.querySelector('.btn-modo5').addEventListener('click', () => { iniciarJuego(5) }, false)
////////////////////////////////////////////////////////////////////////

desplegarJuego = () => {

let escala
let teclasApretadas = {}
let valorPorClick = ''

let atajo = false

//mejorar presión
window.addEventListener('keydown', keyDownListener, false)
function keyDownListener(event) { teclasApretadas[event.key] = true }
window.addEventListener('keyup', keyUpListener, false);
function keyUpListener(event) { teclasApretadas[event.key] = false }

setPantalla = () => {
	var w = window,
	d = document,
	e = d.documentElement,
	g = d.getElementsByTagName('body')[0],
	window_width = w.innerWidth||e.clientWidth||g.clientWidth,
	window_height = w.innerHeight||e.clientHeight||g.clientHeight
	pantalla.x = window_width
	pantalla.y = window_height
	if(pantalla.x > pantalla.y) { pantalla.orientacion = "landscape" } 
	else { pantalla.orientacion = "portrait" }
}

setLado = () => {
	if(pantalla.orientacion === "landscape") { 
		if( pantalla.x >= pantalla.y + pantalla.y / 5 ) {
			
			if( pantalla.x >= pantalla.y + pantalla.y / 5 * 2) {
				pantalla.relacion = "larga"
			}
			else {
				pantalla.relacion = "corta"
			}
			pantalla.lado = pantalla.y
		} else {
			pantalla.lado = (pantalla.x * 5 / 6)
			pantalla.relacion = "corta"
		}
	}
	else { //portrait
		if( pantalla.y >= pantalla.x + pantalla.x / 5 ) {
			if( pantalla.y >= pantalla.x + pantalla.x / 5 * 2) {
				pantalla.relacion = "larga"
			}
			else {
				pantalla.relacion = "corta"
			}
			pantalla.lado = pantalla.x
		} else {
			pantalla.lado = (pantalla.y * 5 / 6 )
			pantalla.relacion = "corta"
		}
	}	
}

getMedioActor = (id) => {
	var actor = actores.filter( (a) => {
		if(a.id === id) { return true}
	})
	return {"x": actor[0].x + escala/2, "y": actor[0].y + escala/2}
}

//lado por segundo
setVelocidad = (vel) => { juego.velocidad = pantalla.lado * vel }	

setEscala = (esc) => { escala = pantalla.lado * esc }

getValor = (limite) => { return Math.floor(Math.random() * limite) }

setPausa = () => {
	if(!pausa) {
		pausa = true
		setStyles('.botonera', {'display': 'none'})
		setStyles('.botonera5', {'display': 'none'})
		setStyles('#botoneraObjetivo', {'display': 'none'})
		setStyles('.botoneraPausa', {'display': 'grid'})
	} 
	else {
		if(juego.activo) {
			setStyles('.botoneraPausa', {'display': 'none'})
			if(juego.mecanica === "simbolos") {
				setStyles('.botonera', {'display': 'grid'})
				setStyles('.botonera5', {'display': 'grid'})
			}
			if(juego.mecanica === "objetivos") {
				setStyles('#botoneraObjetivo', {'display': 'grid'})
			}

			pausa = false
		}
	}
}

setMecanica = (mecanica) => {
	juego.mecanica = mecanica
	if(juego.mecanica === 'simbolos')	{
		setStyles('#botoneraObjetivo', {'display': 'none'})
		setStyles('.botonera', {'display': 'grid'})
		setStyles('.botonera5', {'display': 'grid'})
	}
	if(juego.mecanica === 'objetivos') {
		setStyles('.botonera', {'display': 'none'})
		setStyles('.botonera5', {'display': 'none'})
		setStyles('#botoneraObjetivo', {'display': 'grid'})
	}
}
	
setPantalla()
setLado()
setVelocidad(0.1)
juego.velocidadInicial = 0.1
juego.velocidadFinal = 0.75
setEscala(0.2)


iconoStart.velocidad = juego.velocidad
iconoStart.x = getValor(pantalla.lado - escala * 2)
iconoStart.y = getValor(pantalla.lado - escala * 2)
console.log(escala)

let medio = {"x": pantalla.lado/2, "y": pantalla.lado/2 }

agregarElemento('body', '<div id="contenedorPrincipal2">El modo landscape será agregado en futuras actualizaciones</div>')

	agregarElemento('body', '<div id="contenedorPrincipal"></div>')

	if(pantalla.orientacion === "portrait") {
		if( pantalla.relacion === "corta" ) {
			setStyles("#contenedorPrincipal", {'width': pantalla.lado + 'px', 'grid-template-rows': pantalla.lado + 'px auto ' + pantalla.lado / 5 + 'px'})
		}
		else { //larga
			if(juego.modo === 3 ) {
				setStyles("#contenedorPrincipal", {'width': pantalla.lado + 'px', 'grid-template-rows': pantalla.lado + 'px auto ' + pantalla.lado / 5 + 'px'})
			}
			else { //modo 5
				setStyles("#contenedorPrincipal", {'width': pantalla.lado + 'px', 'grid-template-rows': pantalla.lado + 'px auto ' + pantalla.lado / 5 + 'px ' + pantalla.lado / 5 + 'px'})	
			}
		}
	}
	else { //landscape
		if( pantalla.relacion === "corta" ) {
			setStyles("#contenedorPrincipal", {'height': pantalla.lado + 'px', 'grid-template-columns': pantalla.lado + 'px auto ' + pantalla.lado / 5 + 'px'})
		}
		else {
			
			if(juego.modo === 3 ) {
				setStyles("#contenedorPrincipal", {'height': pantalla.lado + 'px', 'grid-template-columns': pantalla.lado + 'px auto ' + pantalla.lado / 5 + 'px'})
			}
			else { //modo 5
				setStyles("#contenedorPrincipal", {'height': pantalla.lado + 'px', 'grid-template-columns': pantalla.lado + 'px auto ' + pantalla.lado / 5 + 'px ' + pantalla.lado / 5 + 'px'})
			}
		}
	}

	agregarElemento('#contenedorPrincipal', '<div id="areaJuego"></div>')
		agregarElemento('#areaJuego', '<canvas id="canvas" width="' + pantalla.lado + '" height="' + pantalla.lado + '"></canvas>')
	agregarElemento('#contenedorPrincipal', '<div></div>')
	agregarElemento('#contenedorPrincipal', '<div class="areaControles"></div>')

	//Pantalla corta
	if( pantalla.relacion === "corta" ) {
		if( juego.modo === 3 ) {
			agregarElemento('.areaControles', '<div class="botonera3 botonera"></div>')
				agregarElemento('.botonera3', '<div class="btn-piedra img-piedra"><div class="teclasPC">A</div></div>')
				agregarElemento('.botonera3', '<div class="btn-papel img-papel"><div class="teclasPC">S</div></div>')
				agregarElemento('.botonera3', '<div class="btn-tijeras img-tijeras"><div class="teclasPC">D</div></div>')
				
			if(pantalla.orientacion === "portrait") {
				setStyles('.btn-piedra', {'grid-column': 1})
				setStyles('.btn-papel', {'grid-column': 2})
				setStyles('.btn-tijeras', {'grid-column': 3})
			}			
			else { //landscape
				setStyles('.btn-piedra', {'grid-row': 1})
				setStyles('.btn-papel', {'grid-row': 2})
				setStyles('.btn-tijeras', {'grid-row': 3})
			}
		}
		else { //5
			agregarElemento('.areaControles', '<div class="botonera5 botonera"></div>')
				agregarElemento('.botonera5', '<div class="btn-piedra img-piedra"><div class="teclasPC">A</div></div>')
				agregarElemento('.botonera5', '<div class="btn-papel img-papel"><div class="teclasPC">S</div></div>')
				agregarElemento('.botonera5', '<div class="btn-tijeras img-tijeras"><div class="teclasPC">D</div></div>')
				agregarElemento('.botonera5', '<div class="btn-lagarto img-lagarto"><div class="teclasPC">F</div></div>')
				agregarElemento('.botonera5', '<div class="btn-spock img-spock"><div class="teclasPC">G</div></div>')
				
			if(pantalla.orientacion === "portrait") {
				setStyles('.btn-piedra', {'grid-column': 1})
				setStyles('.btn-papel', {'grid-column': 2})
				setStyles('.btn-tijeras', {'grid-column': 3})
				setStyles('.btn-lagarto', {'grid-column': 4})
				setStyles('.btn-spock', {'grid-column': 5})
			}
			else { //landscape
				setStyles('.btn-piedra', {'grid-row': 1})
				setStyles('.btn-papel', {'grid-row': 2})
				setStyles('.btn-tijeras', {'grid-row': 3})
				setStyles('.btn-lagarto', {'grid-row': 4})
				setStyles('.btn-spock', {'grid-row': 5})
			}
		}
	}
	else { //Pantalla larga
		if( juego.modo === 3 ) {
			agregarElemento('.areaControles', '<div class="botonera3 botonera"></div>')
				agregarElemento('.botonera3', '<div class="btn-piedra img-piedra"><div class="teclasPC">A</div></div>')
				agregarElemento('.botonera3', '<div class="btn-papel img-papel"><div class="teclasPC">S</div></div>')
				agregarElemento('.botonera3', '<div class="btn-tijeras img-tijeras"><div class="teclasPC">D</div></div>')
				
			if(pantalla.orientacion === "portrait") {
				setStyles('.btn-piedra', {'grid-column': 1})
				setStyles('.btn-papel', {'grid-column': 2})
				setStyles('.btn-tijeras', {'grid-column': 3})
			}
			else { //landscape
				setStyles('.btn-piedra', {'grid-row': 1})
				setStyles('.btn-papel', {'grid-row': 2})
				setStyles('.btn-tijeras', {'grid-row': 3})
			}
		}
		else { // 5
			agregarElemento('.areaControles', '<div class="botonera3 botonera"></div>')
				agregarElemento('.botonera3', '<div class="btn-piedra img-piedra"><div class="teclasPC">A</div></div>')
				agregarElemento('.botonera3', '<div class="btn-papel img-papel"><div class="teclasPC">S</div></div>')
				agregarElemento('.botonera3', '<div class="btn-tijeras img-tijeras"><div class="teclasPC">D</div></div>')
				
			agregarElemento('#contenedorPrincipal', '<div class="areaControles2"></div>')
			agregarElemento('.areaControles2:last-child', '<div class="botonera5"><div class="teclasPC"></div></div>')
				agregarElemento('.botonera5', '<div class="btn-lagarto img-lagarto"><div class="teclasPC">F</div></div>')
				agregarElemento('.botonera5', '<div class="btn-spock img-spock"><div class="teclasPC">G</div></div>')
				agregarElemento('.botonera5', '<div class="vacio"></div>')

			if(pantalla.orientacion === "portrait") {
				setStyles('.btn-piedra', {'grid-column': 1})
				setStyles('.btn-papel', {'grid-column': 2})
				setStyles('.btn-tijeras', {'grid-column': 3})
				setStyles('.btn-lagarto', {'grid-column': 2})
				setStyles('.btn-spock', {'grid-column': 4})
				setStyles('.vacio', {'grid-column': 5})
			}
			else { //landscape
				setStyles('.btn-piedra', {'grid-row': 1})
				setStyles('.btn-papel', {'grid-row': 2})
				setStyles('.btn-tijeras', {'grid-row': 3})
				setStyles('.btn-lagarto', {'grid-row': 2})
				setStyles('.btn-spock', {'grid-row': 4})
				setStyles('.vacio', {'grid-row': 5})
			}
		}
	}
	
///////
	agregarElemento('.areaControles', '<div class="botoneraPausa"></div>')
		agregarElemento('.botoneraPausa', '<div class="btn-pausa-salir img-pausa-salir"></div>')
		agregarElemento('.botoneraPausa', '<div class="btn-pausa-reiniciar img-pausa-reiniciar"></div>')
	if(pantalla.orientacion === "portrait") {
		setStyles('.btn-pausa-salir', {'grid-column': 1})
		setStyles('.btn-pausa-reiniciar', {'grid-column': 3})
	}
	else { //landscape
		setStyles('.btn-pausa-salir', {'grid-row': 1})
		setStyles('.btn-pausa-reiniciar', {'grid-row': 3})
	}

	agregarElemento('.areaControles', '<div class="botoneraObjetivo" id="botoneraObjetivo"></div>')
		agregarElemento('.botoneraObjetivo', '<div class="btn-objetivo-ganar img-objetivo-ganar"></div>')
		agregarElemento('.botoneraObjetivo', '<div class="btn-objetivo-empatar img-objetivo-empatar"></div>')
		agregarElemento('.botoneraObjetivo', '<div class="btn-objetivo-perder img-objetivo-perder"></div>')
		setStyles('.btn-objetivo-ganar', {'grid-column': 1})
		setStyles('.btn-objetivo-empatar', {'grid-column': 2})
		setStyles('.btn-objetivo-perder', {'grid-column': 3})
///////
	
	//Botones
	document.querySelector('.btn-piedra').addEventListener('click', () => { valorPorClick = 'piedra' }, false)
	document.querySelector('.btn-papel').addEventListener('click', () => { valorPorClick = 'papel' }, false)
	document.querySelector('.btn-tijeras').addEventListener('click', () => { valorPorClick = 'tijeras' }, false)
	if( juego.modo === 5 ) {
		document.querySelector('.btn-lagarto').addEventListener('click', () => { valorPorClick = 'lagarto' }, false)
		document.querySelector('.btn-spock').addEventListener('click', () => { valorPorClick = 'spock' }, false)
	}

	setStyles('.img-piedra', {'background-position': '-' + (pantalla.lado / 5) * 0 + 'px 0'})
	setStyles('.img-papel', {'background-position': '-' + (pantalla.lado / 5) * 1 + 'px 0'})
	setStyles('.img-tijeras', {'background-position': '-' + (pantalla.lado / 5) * 2 + 'px 0'})
	setStyles('.img-lagarto', {'background-position': '-' + (pantalla.lado / 5) * 3 + 'px 0'})
	setStyles('.img-spock', {'background-position': '-' + (pantalla.lado / 5) * 4 + 'px 0'})
	
	setStyles('.img-objetivo-ganar', {'background-position': '0px -' + (pantalla.lado / 5) * 0 + 'px' })
	setStyles('.img-objetivo-empatar', {'background-position': '0px -' + (pantalla.lado / 5) * 1 + 'px' })
	setStyles('.img-objetivo-perder', {'background-position': '0px -' + (pantalla.lado / 5) * 2 + 'px' })


	document.querySelector('.img-objetivo-ganar').addEventListener('click', () => { valorPorClick = 'ganar' }, false)
	document.querySelector('.img-objetivo-empatar').addEventListener('click', () => { valorPorClick = 'empatar' }, false)
	document.querySelector('.img-objetivo-perder').addEventListener('click', () => { valorPorClick = 'perder' }, false)


	setStyles('.img-pausa-salir', {'background-position': '-' + (pantalla.lado / 5) * 0 + 'px 0' })
	setStyles('.img-pausa-reiniciar', {'background-position': '-' + (pantalla.lado / 5) * 1 + 'px 0' })
	
	document.querySelector('.btn-pausa-salir').addEventListener('click', () => { location.reload() }, false)
	document.querySelector('.btn-pausa-reiniciar').addEventListener('click', () => { iniciarJuego(juego.modo) }, false)

	document.querySelector('#canvas').addEventListener('click', () => { setPausa() }, false)


if( esMobile() ) { setStylesAll('.teclasPC', {'display': 'none'}) }

////////////////////////////////////////////////////////////////////////


	let canvas = document.getElementById("canvas")
	let ctx = canvas.getContext("2d")
	ctx.fillStyle = 'black'
	//ctx.fillStyle = 'red'
	ctx.fillRect(0,0,canvas.width, canvas.height)

	if(pantalla.orientacion === "portrait") { ctx.font = "8vw Mono" }
	else { ctx.font = "8vh Mono" }

	let metas = {"x": medio.x - (escala*0.75)/2, "y": pantalla.lado - escala*0.75, "objetivo": 0 }
	//metas.objetivo = getValor(3) //primara vez
metas.objetivo = 1

	setSombra = () => {
		switch(metas.objetivo) {
			case 0: metas.sombra = 'green'; break
			case 1: metas.sombra = 'white'; break
			case 2: metas.sombra = 'red'; break
		}
	}

	setSombra()

let actores = 
	[
	  {"id": 0, "x": 0, "y" :0, "xInicial":0 , "yInicial":0, "activo": true, "valor":""},
	  {"id": 1, "x": pantalla.lado - escala, "y" :0, "xInicial": pantalla.lado - escala, "yInicial" :0, "activo": false, "valor":""},
	  {"id": 2, "x": pantalla.lado - escala, "y" :pantalla.lado - escala, "xInicial": pantalla.lado - escala, "yInicial" :pantalla.lado - escala, "activo": false, "valor":""},
	  {"id": 3, "x": 0, "y" : pantalla.lado - escala, "xInicial": 0, "yInicial" : pantalla.lado - escala, "activo": false, "valor":""}
	]

let centro = {"valor": 0, "limite": juego.modo}
var grd = false


//carga
actores[0].valor = getValor(juego.modo)

rotarActores = (id) => {
	actores.map( (a) => {
		if(a.id === id) {
			 a.activo = false }

		if( a.id === 0 && id === 3 || 
			a.id === id + 1) {
			a.x = a.xInicial
			a.y = a.yInicial
			a.valor = getValor(juego.modo)
			a.activo = true
		}
	})
}

//mueve los actores
mover = () => {
	actores.map( (a) => {
		if(a.activo) {
			if(a.x < medio.x) { a.x = a.x + (juego.velocidad * juego.delta) }
			if(a.x > medio.x) { a.x = a.x - (juego.velocidad * juego.delta) }
			if(a.y < medio.y) { a.y = a.y + (juego.velocidad * juego.delta) }
			if(a.y > medio.y) { a.y = a.y - (juego.velocidad * juego.delta) }
		}
	})
}

let distanciaMax = Math.sqrt( Math.pow( (escala / 2 - medio.x) , 2) + Math.pow( (escala / 2 - medio.y) , 2) )
let acercamiento = 1

//toma decisiones
procesar = () => {
	actores.map( (a) => {
		if(a.activo) {
			
			var medioActor = getMedioActor(a.id)
			var distancia = Math.sqrt( Math.pow( (medioActor.x - medio.x) , 2) + Math.pow( (medioActor.y - medio.y) , 2) )

			acercamiento = (distancia - escala) / (distanciaMax - escala)

			if(atajo === true) {				
				if(metas.objetivo === 0) { //ganar
					centro.valor = (getGanadores(a.valor))[getValor(2)]
				}
				if(metas.objetivo === 1) { //empatar
					centro.valor = a.valor
				}
				if(metas.objetivo === 2) { //perder
					centro.valor = (getPerdedores(a.valor))[getValor(2)]
				}
				atajo = false
				//setVelocidad(0.75)
			}
	
			//cambiar escala por tamanio centro
			if(distancia < escala) {
				//llevar a función para procesar color borde tambien
				var flagSumaPuntos = false
				if(centro.valor === a.valor) { //empata
					if(metas.objetivo === 1) { //debe empatar
						juego.puntaje++; flagSumaPuntos= true; console.log('bien empata') } else { juego.vidas--; console.log('mal empata') }
				} 
				else { //no empata
					if(	esGanador(centro.valor, a.valor) ) { //gana
						if(metas.objetivo === 0) { //debe ganar
							juego.puntaje++; flagSumaPuntos= true; console.log('bien gana') } else { juego.vidas--; console.log('mal gana') }
					}
					else { //pierde
						if(metas.objetivo === 2) { //debe perder
							juego.puntaje++; flagSumaPuntos= true; console.log('bien3') } else { juego.vidas--; console.log('mal pierde') }
					}
				}
					//control de fin de juego
				if(juego.vidas === 0) { juego.activo = false }
				else {

					rotarActores(a.id)
					//¿objetivos por puntajes?
					if (juego.puntaje >= 15) {

					//ajustes de puntos
					if(flagSumaPuntos) {
						juego.velocidadInicial += juego.velocidadInicial * 0.05;
						if(juego.velocidadInicial < juego.velocidadFinal) {
							setVelocidad(juego.velocidadInicial)
						} else { setVelocidad(juego.velocidadFinal) }
					} else {
						juego.velocidadInicial = 0.1
						setVelocidad(juego.velocidadInicial)
					}

						if(juego.puntaje % 3 === 0) {
							setMecanica("objetivos")
						}
						else {
							metas.objetivo = getValor(3) 
							setMecanica("simbolos")						
						}
					}
					else {
						if (juego.puntaje < 5) { metas.objetivo = 1 }
						if (juego.puntaje >= 5 && juego.puntaje < 10) { metas.objetivo = 0 }
						if (juego.puntaje >= 10 && juego.puntaje < 15) { metas.objetivo = 2 }
					}
					setSombra()
					/*
					switch(metas.objetivo) {
						case 0: metas.sombra = 'green'; break
						case 1: metas.sombra = 'white'; break
						case 2: metas.sombra = 'red'; break
					}
					*/
				}

			}//colision
		}
	})//actores.map
}//procesar

//dibuja lo del canvas
dibujar = () => {
	
	//limpiar
	ctx.fillStyle = 'black'

if(grd) {
	grd.addColorStop(0, "grey");
	grd.addColorStop(1, "black");
	ctx.fillStyle = grd;
}

	ctx.fillRect(0,0,canvas.width, canvas.height)
	
	let a = actores.filter( (a) => { return a.activo === true } )[0]
	

	//desde las esquinas
	ctx.drawImage(sprites, a.valor*200, 0, 200, 200, a.x, a.y, escala, escala)

	//centro
	ctx.drawImage(sprites, centro.valor*200, 0, 200, 200, medio.x-escala/2, medio.y-escala/2, escala, escala)

	//metas
	ctx.shadowBlur=escala/10
	ctx.shadowColor=metas.sombra
	ctx.drawImage(metasSprites, 0, metas.objetivo*200, 200, 200, metas.x, metas.y, escala*0.75, escala*0.75)
	//


	//puntaje y vidas
	ctx.shadowBlur=0
	ctx.fillStyle = "#FFFFFF";
	ctx.textAlign = "center";
	ctx.textBaseline = "top";
	ctx.fillText(juego.puntaje, pantalla.lado/2, 0);
	ctx.textBaseline = "center";
	ctx.fillText( "FIN".slice(0, 3 - juego.vidas) , pantalla.lado/2, pantalla.lado*0.75);

	//semicirculo central
	if(juego.activo) {
		ctx.strokeStyle = metas.sombra
		ctx.lineWidth = escala / 20
		ctx.beginPath()
		var angle = 0
			grd=false
			if(a.id === 0) {
				ctx.arc(medio.x,medio.y,escala*0.55, - Math.PI * 0.75 + Math.PI * (1 - acercamiento) , Math.PI * 1.25 - Math.PI * (1 - acercamiento) )
				grd = ctx.createLinearGradient(0,0,pantalla.lado * (1-acercamiento) * 0.5, pantalla.lado * (1-acercamiento) * 0.5);
			}
			if(a.id === 1) {
				ctx.arc(medio.x,medio.y,escala*0.55, - Math.PI * 0.25 + Math.PI * (1 - acercamiento) , Math.PI * 1.75 - Math.PI * (1 - acercamiento) )
				grd = ctx.createLinearGradient(pantalla.lado,0,pantalla.lado * 0.5 +  pantalla.lado * 0.5 * (acercamiento), pantalla.lado * (1-acercamiento) * 0.5)
			}
			if(a.id === 2) {
				ctx.arc(medio.x,medio.y,escala*0.55, Math.PI * 0.25 + Math.PI * (1 - acercamiento) , - Math.PI * 1.75 - Math.PI * (1 - acercamiento) )
				grd = ctx.createLinearGradient(pantalla.lado,pantalla.lado,pantalla.lado * 0.5 +  pantalla.lado * 0.5 * (acercamiento), pantalla.lado * 0.5 +  pantalla.lado * 0.5 * (acercamiento))
			}
			if(a.id === 3) {
				ctx.arc(medio.x,medio.y,escala*0.55, Math.PI * 0.75 + Math.PI * (1 - acercamiento) , - Math.PI * 1.25 - Math.PI * (1 - acercamiento) )
				grd = ctx.createLinearGradient(0,pantalla.lado,pantalla.lado * (1-acercamiento) * 0.5, pantalla.lado * 0.5 +  pantalla.lado * 0.5 * (acercamiento))
			}
		ctx.stroke()
	}
}

analizarInput = () => {
	if (teclasApretadas.a || valorPorClick === 'piedra')	{ centro.valor = 0 }
	if (teclasApretadas.s || valorPorClick === 'papel')		{ centro.valor = 1 }
	if (teclasApretadas.d || valorPorClick === 'tijeras') { centro.valor = 2 }
	if (teclasApretadas.f || valorPorClick === 'lagarto') { centro.valor = 3 }
	if (teclasApretadas.g || valorPorClick === 'spock') 	{ centro.valor = 4 }
//Cambio de objetivos
	if (teclasApretadas.z || valorPorClick === 'ganar') 	{ metas.objetivo = 0 }
	if (teclasApretadas.x || valorPorClick === 'empatar') { metas.objetivo = 1 }
	if (teclasApretadas.c || valorPorClick === 'perder') 	{ metas.objetivo = 2 }
	setSombra()

	if (teclasApretadas.q) { atajo = true	}
	if(valorPorClick !== '') { valorPorClick = '' }	
	//setVelocidad(juego.velocidad + juego.velocidadInicial);
}//analizarInput

dibujarEnPausa = () => {
	ctx.fillStyle = 'grey'
	ctx.fillRect(0,0,canvas.width, canvas.height)
	if(juego.delta > 0) {
		iconoStart.x = iconoStart.x + iconoStart.dir_x * (iconoStart.velocidad * 2 * juego.delta)
		iconoStart.y = iconoStart.y + iconoStart.dir_y * (iconoStart.velocidad * 2 * juego.delta)
		if(iconoStart.x > pantalla.lado - escala * 2) iconoStart.dir_x *= -1
		if(iconoStart.y > pantalla.lado - escala * 2) iconoStart.dir_y *= -1
		if(iconoStart.x < 0) iconoStart.dir_x *= -1
		if(iconoStart.y < 0) iconoStart.dir_y *= -1
	}
	ctx.drawImage(iconoStart.imagen, 0, 0, 200, 200, iconoStart.x, iconoStart.y, escala * 2, escala * 2)
}//dibujarEnPausa

let lastLoop = (new Date()).getMilliseconds()

gameLoop = () => {
	juego.delta = ((new Date()).getMilliseconds() - lastLoop) / 1000
	lastLoop = (new Date()).getMilliseconds()
	if(juego.delta > 0 && (!pausa && !pausaLandscape)) {
		analizarInput()
		mover()
		procesar()
		dibujar()
	} 
	if(juego.delta > 0 && (pausa || pausaLandscape) ) {
		dibujarEnPausa()
	}
	if(juego.activo) { window.requestAnimationFrame(gameLoop) }
	else {
		setPausa()

		if(juego.modo === 3) {
			console.log("puntos " + API_puntajeMinimoModo3)

			if(API_puntajeMinimoModo3 === undefined) { API_puntajeMinimoModo3 = 5 }
			if(juego.puntaje > API_puntajeMinimoModo3 ) {
				var nombreJugador = prompt("Nuevo record en el Modo 3 - Ingrese su nombre")
				nombreJugador = normalizarAcentos(nombreJugador)
				API_mandarPuntaje ( 'modo3', nombreJugador , juego.puntaje)
			}
		}

		if(juego.modo === 5) {
			if(API_puntajeMinimoModo5 === undefined) { API_puntajeMinimoModo5 = 5 }
			if(juego.puntaje > API_puntajeMinimoModo5 ) {
				var nombreJugador = prompt("Nuevo record en el Modo 5 - Ingrese su nombre")
				nombreJugador = normalizarAcentos(nombreJugador)
				API_mandarPuntaje ( 'modo5', nombreJugador , juego.puntaje)
			}
		}

	}
}//gameLoop

gameLoop()

}
